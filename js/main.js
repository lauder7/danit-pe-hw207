aWords = ["John", "Irene", 45, "Anna", "Peter", 67, null, [100, 101, 102], {sPN: "CN785JX", sProduct: "Girl's Yellow Dress"}, "Franz"];
console.log(filterBy(aWords, "number"));
console.log(filterBy(aWords, "string"));
console.log(filterBy(aWords, "object"));

function filterBy(aArray, sType) {
    return aArray.filter(item => typeof(item) !== sType);
}